package info.textgrid.consistency;

/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/*																		*/
/* HttpMethods for Consistency Checker */
/* Author: Fatih Berber */
/* Email: fatih.berber@gwdg.de */
/* ORCID: orcid.org/0000-0001-5507-4048 */
/*																		*/
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import javax.xml.bind.JAXB;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;

/**
 *
 */
public class HttpMethods {

  private static final int EPIC_PORT = 80;

  private String baseRoute;
  private String pidPrefix;
  private Logger logger;
  private CloseableHttpClient httpClient;
  private HttpClientContext context;
  private CredentialsProvider credsProvider = new BasicCredentialsProvider();
  private ArrayList<String> textGridHandles = new ArrayList<String>();

  /**
   * @param logger
   * @param pidPrefix
   * @param epicUser
   * @param epicUserPw
   * @param epicHost
   * @param epicLimit
   */
  public HttpMethods(Logger logger, String pidPrefix, String epicUser, String epicUserPw,
      String epicHost, String epicLimit) {

    this.logger = logger;

    this.logger.log("preparing http methods");

    this.pidPrefix = pidPrefix;
    this.credsProvider.setCredentials(AuthScope.ANY,
        new UsernamePasswordCredentials(epicUser, epicUserPw));

    AuthCache authCache = new BasicAuthCache();
    HttpHost httpHost = new HttpHost(epicHost, EPIC_PORT);
    authCache.put(httpHost, new BasicScheme());

    this.context = HttpClientContext.create();
    this.context.setCredentialsProvider(this.credsProvider);
    this.context.setAuthCache(authCache);
    this.baseRoute = "http://" + epicHost + "/handles/" + pidPrefix + "?limit=" + epicLimit;
    // this.baseRoute = "http://" + epicHost + "/handles/" + pidPrefix +
    // "/00-1734-0000-0001-4C60-9";

    this.httpClient = HttpClientBuilder.create().build();
  }

  /**
   * @throws IOException
   */
  public void provisionTextGridHandlesList() throws IOException {

    this.logger.log("getting textgrid handle list from epic api: " + this.baseRoute);

    HttpGet httpget = new HttpGet(this.baseRoute);
    httpget.addHeader("Accept", "application/json");

    CloseableHttpResponse response = null;
    try {
      response = this.httpClient.execute(httpget, this.context);

      String jsonListAsString = null;
      int statusCode = response.getStatusLine().getStatusCode();

      if (statusCode != HttpStatus.SC_OK) {
        throw new IOException("http error " + statusCode);
      }

      HttpEntity entity = response.getEntity();
      if (entity != null) {

        this.logger.log("reading the content of http response from epic api");

        InputStream is = entity.getContent();
        jsonListAsString = toStringInputStream(is);
        this.logger.log("parsing handles from content");
        transformJsonStringToList(jsonListAsString);
        is.close();
      }
    } finally {
      response.close();
      this.logger.log("http connection closed");
    }
  }

  /**
   * @param thePath
   * @throws IOException
   */
  public void provisionAndStoreTextGridHandlesListFromDisk(File thePath, FileWriter theFile)
      throws IOException {

    // Loop the path.
    for (File f : thePath.listFiles()) {
      if (f.isDirectory()) {
        provisionAndStoreTextGridHandlesListFromDisk(f, theFile);
      } else {
        if (f.getName().endsWith(".meta")) {
          // Get TG URI and PID from metadata.
          MetadataContainerType metadata = JAXB.unmarshal(f, MetadataContainerType.class);
          // String tguri =
          // metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
          String pid = metadata.getObject().getGeneric().getGenerated().getPid().get(0).getValue()
              .replace("hdl:", "");
          theFile.append(pid + "\n");
        }
      }
    }
  }

  /**
   * @param jsonListAsString
   */
  public void transformJsonStringToList(String jsonListAsString) {

    JsonParser jsonParser = new JsonParser();
    JsonArray jsonArray = (JsonArray) jsonParser.parse(jsonListAsString);

    for (int i = 0; i < jsonArray.size(); i++) {
      JsonElement jsonHandle = jsonArray.get(i);
      String suffix = jsonHandle.getAsString();
      String handle = this.pidPrefix + "/" + suffix;
      this.textGridHandles.add(handle);
    }

    this.logger.log("got " + jsonArray.size() + " pids! yeah!");
  }

  /**
   * @param is
   * @return
   * @throws IOException
   */
  private static String toStringInputStream(InputStream is) throws IOException {

    byte[] buffer = new byte[1024];
    StringBuilder stringBuilder = new StringBuilder();
    int length = 0;

    while ((length = is.read(buffer)) >= 0) {
      stringBuilder.append(new String(Arrays.copyOfRange(buffer, 0, length), "UTF-8"));
    }

    return stringBuilder.toString();
  }

  /**
   * @param pidListPath
   */
  public void storeTextGridHandlesListToFile(String pidListPath) {

    FileWriter output = null;
    try {
      output = new FileWriter(pidListPath, true);
      BufferedWriter writer = new BufferedWriter(output);

      for (int i = 0; i < this.textGridHandles.size(); i++) {
        String handle = this.textGridHandles.get(i);
        writer.write(handle + "\n");
      }

      writer.close();

    } catch (Exception e) {
      System.out.println("filewritererror");
      throw new RuntimeException(e);
    } finally {
      if (output != null) {
        try {
          output.close();
        } catch (IOException e) {
          // Ignore issues during closing
        }
      }
    }
  }

  /**
   * @return
   */
  public int getTextGridHandlesCount() {
    return this.textGridHandles.size();
  }

  /**
   * @param threadCount
   * @return
   */
  public ArrayList<ArrayList<String>> getLegacyHandles(int threadCount) {

    ArrayList<ArrayList<String>> listsPerThread = new ArrayList<ArrayList<String>>();
    int listLength = this.textGridHandles.size();
    int perThreadHandlesCount = listLength / threadCount;
    ArrayList<String> perThreadList = new ArrayList<String>();

    for (int i = 0; i < this.textGridHandles.size(); i++) {
      String hdl = this.textGridHandles.get(i);
      perThreadList.add(hdl);

      if ((i + 1) <= perThreadHandlesCount * threadCount) {
        if (perThreadList.size() == perThreadHandlesCount) {
          listsPerThread.add(perThreadList);
          if (listsPerThread.size() < threadCount) {
            perThreadList = new ArrayList<String>();
          }
        }
      }
    }

    listsPerThread.add(perThreadList);

    return listsPerThread;
  }

}
