package info.textgrid.consistency;

/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/*																		*/
/* TextGridHandleReader for Consistency Checker */
/* Author: Fatih Berber */
/* Email: fatih.berber@gwdg.de */
/* ORCID: orcid.org/0000-0001-5507-4048 */
/*																		*/
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.codec.binary.Hex;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.google.common.io.Files;
import net.handle.hdllib.HandleValue;

/**
 *
 */
public class TextGridHandlesReader implements Runnable {

  // hm? maybe round one and round two?
  private static final String EX1 = "EX-1";
  private static final String EX2 = "EX-2";

  // all file sizes are fine
  private static final String FS_OC_OK = "FS-1";
  // ???
  private static final String FS_OC_FS2 = "FS-2";
  // filesize of file == filesize HandleRecord but filesize in Metadata differs
  private static final String FS_OC_ERR_3000 = "FS-3000";
  // filesize of file == filesize HandleRecord but no filesize in Metadata
  private static final String FS_OC_ERR_3010 = "FS-3010";
  // filesize of file != filesize HandleRecord but filesize Metadata == filesize File
  private static final String FS_OC_ERR_3020 = "FS-3020";
  // filesize of file != filesize HandleRecord and filesize Metadata != filesize File (all
  // different)
  private static final String FS_OC_ERR_3030 = "FS-3030";
  // filesize of file != filesize HandleRecord and filesize Metadata == NULL
  private static final String FS_OC_ERR_3040 = "FS-3040";
  // filesize of file == filesize Metadata but no filesize in HandleRecord
  private static final String FS_OC_ERR_3050 = "FS-3050";
  // filesize of file != filesize Metadata and no filesize in HandleRecord
  private static final String FS_OC_ERR_3060 = "FS-3060";
  // no filesize in Metadata and no filesize in HandleRecord
  private static final String FS_OC_ERR_3070 = "FS-3070";

  // all checksum are fine
  private static final String CS_OC_OK = "CS-1";
  // ???
  private static final String CS_OC_CS2 = "CS-2";
  // checksum of file == checksum HandleRecord but checksum in Metadata differs
  private static final String CS_OC_ERR_2000 = "CS-2000";
  // checksum of file == checksum HandleRecord but no checksum in Metadata
  private static final String CS_OC_ERR_2010 = "CS-2010";
  // checksum of file != checksum HandleRecord but checksum Metadata == checksum File
  private static final String CS_OC_ERR_2020 = "CS-2020";
  // checksum of file != checksum HandleRecord and checksum Metadata != checksum File (all
  // different)
  private static final String CS_OC_ERR_2030 = "CS-2030";
  // checksum of file != checksum HandleRecord and checksum Metadata == NULL
  private static final String CS_OC_ERR_2040 = "CS-2040";
  // checksum of file == checksum Metadata but no checksum in HandleRecord
  private static final String CS_OC_ERR_2050 = "CS-2050";
  // checksum of file != checksum Metadata and no checksum in HandleRecord
  private static final String CS_OC_ERR_2060 = "CS-2060";
  // no checksum in Metadata and no checksum in HandleRecord
  private static final String CS_OC_ERR_2070 = "CS-2070";

  // check file sizes only. file sizes OK.
  private static final String OK_FS_ONLY = "FSO-OK";
  // check file sizes only. file sizes differ.
  private static final String WARN_FS_ONLY_01 = "FSO-E1";
  // check file sizes only. no metadata file size.
  private static final String WARN_FS_ONLY_02 = "FSO-E2";

  private static final String FILESIZEFIELD_EXTENT = "ns3:extent";
  private static final String CHECKSUMFIELD_FIXITY = "ns3:fixity";
  private static final String CHECKSUMFIELD_CHECKSUM = "ns3:messageDigest";
  private static final String ITEM_CHAR = "=";
  // Use CSV format here.
  private static final String SEPA_CHAR = ",";

  private Logger logger;
  private ArrayList<String> textGridHandles;
  private HandleMethods hs;
  private String textGridStoragePath;
  private int runMethod;

  private HashMap<Long, Long> categories = new HashMap<Long, Long>();

  /**
   * @param logger
   * @param textGridHandles
   * @param hs
   * @param textGridStoragePath
   * @param filesizesOnly
   */
  public TextGridHandlesReader(Logger logger, ArrayList<String> textGridHandles, HandleMethods hs,
      String textGridStoragePath, boolean filesizesOnly) {
    this.logger = logger;
    this.textGridHandles = textGridHandles;
    this.hs = hs;
    this.textGridStoragePath = textGridStoragePath;
    if (filesizesOnly) {
      this.runMethod = 3;
    } else {
      this.runMethod = 1;
    }
  }

  /**
   * @param logger
   * @param textGridHandles
   */
  public TextGridHandlesReader(Logger logger, ArrayList<String> textGridHandles, HandleMethods hs) {
    this.logger = logger;
    this.textGridHandles = textGridHandles;
    this.hs = hs;
    this.runMethod = 2;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    Iterator<String> it = this.textGridHandles.iterator();
    while (it.hasNext()) {
      String handle = it.next();
      HandleValue[] values = this.hs.readHandle(handle);
      if (values != null && values.length > 0) {
        if (this.runMethod == 1) {
          examineAndProcessHandleRecord(values, handle);
        } else if (this.runMethod == 2) {
          examineHandleRecord(values, handle);
        } else if (this.runMethod == 3) {
          examineAndProcessFileSizeOnly(values, handle);
        }
      }
    }
  }

  /**
   * <p>
   * This is method No1!
   * </p>
   * 
   * @param values
   * @param handle
   */
  public void examineAndProcessHandleRecord(HandleValue[] values, String handle) {

    this.logger.log("examining and processing textgrid handle record: " + handle);

    TextGridRecord tgRecord = new TextGridRecord(this.logger);

    boolean isTextGridRelated = false;

    for (int i = 0; i < values.length; i++) {

      String theType = values[i].getTypeAsString();
      String theData = values[i].getDataAsString();

      if (theType.equals("URL")) {
        if (tgRecord.urlHasTextGridPattern(theData)) {
          isTextGridRelated = true;
          tgRecord.setUri(theData);
        } else {
          isTextGridRelated = false;
        }
      } else if (theType.equals("FILESIZE")) {
        tgRecord.setFileSizeFromHandleRecord(theData);
      } else if (theType.equals("CHECKSUM")) {
        tgRecord.setCheckSumFromHandleRecord(theData);
      }
    }

    int checksumFlag = -1;
    String checksumOpCode = null;
    String checksumMessage = null;

    int filesizeFlag = -1;
    String filesizeOpCode = null;
    String filesizeMessage = null;

    if (isTextGridRelated) {
      if (tgRecord.hasUrl) {
        String objectPath = tgRecord.getObjectPath(this.textGridStoragePath);
        if (objectPath != null) {
          try {

            this.logger.log("reading file from: " + objectPath);

            File tgObject = new File(objectPath);
            int objectExists = 0;
            if (tgObject.exists() && !tgObject.isDirectory()) {

              this.logger.log("tg object exists:" + tgRecord.getUri());

              objectExists = 1;
            } else {
              this.logger.log("tg object is not existing");
              this.logger.check_detailed(handle, tgRecord.getUri(), 0, EX2, 0, FS_OC_FS2, 0,
                  CS_OC_CS2);
            }

            if (objectExists == 1) {
              byte[] tgObjectBytes = Files.toByteArray(tgObject);

              this.logger.log("extracting filesize and checksum from metadata");

              FileSizeAndCheckum fsCs = TextGridMetaDataFilesizeAndChecksum(objectPath + ".meta");
              String metadataChecksum = fsCs.checksum;
              int metadataFilesize = fsCs.filesize;
              String tgObjectCheckumFromFile =
                  calculateMD5Checksum(tgObjectBytes, tgRecord.getUri());
              int sizeOnDisk = tgObjectBytes.length;

              /* FILESIZE CHECKS */
              if (tgRecord.hasFileSizeFromHandleRecord) {
                /* there is a filesize in HandleRecord */
                int sizeInHdlRecord = tgRecord.getFileSizeFromHandleRecord();

                if (sizeOnDisk == sizeInHdlRecord) {
                  /* filesize in HandleRecord == filesize of File */

                  if (metadataFilesize > -1) {
                    /* there is a checksum in Metadata */

                    if (metadataFilesize == sizeOnDisk) {
                      /* all filesizes are OK */
                      filesizeFlag = 1;
                      filesizeOpCode = FS_OC_OK;
                      filesizeMessage = "OK-FS(1): ALL THREE FILESIZES ARE EQUAL";
                      this.logger.log(filesizeMessage);
                    } else {
                      /* filesize in Metadata not OK */
                      filesizeFlag = 0;
                      filesizeOpCode = FS_OC_ERR_3000;
                      filesizeMessage =
                          "WARNING-FS(3000): FILESIZE IN METADATA DIFFERS FROM FILE AND HandleRecord";
                      this.logger.log(filesizeMessage);
                    }
                  } else {
                    /* there is no filesize in Metadata */
                    filesizeFlag = 0;
                    filesizeOpCode = FS_OC_ERR_3010;
                    filesizeMessage =
                        "WARNING-FS(3010): FILESIZE IN METADATA IS NULL BUT HandleRecord and File EQUAL";
                    this.logger.log(filesizeMessage);
                  }
                } else {
                  /* filesize in HandleRecord != filesize of File */

                  if (metadataFilesize > -1) {
                    /* there is a filesize in Metadata */

                    if (metadataFilesize == sizeOnDisk) {
                      /* filesize in Metadata == filesize of File */
                      filesizeFlag = 0;
                      filesizeOpCode = FS_OC_ERR_3020;
                      filesizeMessage =
                          "WARNING-FS(3020): FILESIZE IN HandleRecord Differs FROM FILE AND METADATA";
                      this.logger.log(filesizeMessage);
                    } else {
                      filesizeFlag = 0;
                      filesizeOpCode = FS_OC_ERR_3030;
                      filesizeMessage = "WARNING-FS(3030): ALL FILESIZES ARE DIFFERENT";
                      this.logger.log(filesizeMessage);
                    }
                  } else {
                    /* there is no filesize in Metadata */
                    /* and filesize of file and HandleRecord not equal */
                    filesizeFlag = 0;
                    filesizeOpCode = FS_OC_ERR_3040;
                    filesizeMessage =
                        "WARNING-FS(3040): FILESIZE IN METADATA IS NULL AND HandleRecord != File EQUAL";
                    this.logger.log(filesizeMessage);
                  }
                }
              } else {
                /* no filesize in HandleRecord */
                if (metadataFilesize > -1) {
                  /* there is a filesize in Metadata */

                  if (metadataFilesize == sizeOnDisk) {
                    /* checksum in filesize == filesize of File */

                    filesizeFlag = 0;
                    filesizeOpCode = FS_OC_ERR_3050;
                    filesizeMessage =
                        "WARNING-FS(3050): (NO FILESIZE IN HandleRecord) FILESIZE IS EQUAL FOR FILE AND METADATA";
                    this.logger.log(filesizeMessage);
                  } else {
                    /* filesize in Metadata != filesize of File */
                    filesizeFlag = 0;
                    filesizeOpCode = FS_OC_ERR_3060;
                    filesizeMessage =
                        "WARNING-FS(3060): (NO FILESIZE IN HandleRecord) ALL FILESIZE ARE DIFFERENT";
                    this.logger.log(filesizeMessage);
                  }
                } else {
                  /* there is no filesize in Metadata (and no filesize in HandleRecord) */
                  /* and filesize of file and HandleRecord not equal */

                  filesizeFlag = 0;
                  filesizeOpCode = FS_OC_ERR_3070;
                  filesizeMessage =
                      "WARNING-FS(3070): (NO FILESIZE IN HandleRecord) (NO FILESIZE IN METADATA)";
                  this.logger.log(filesizeMessage);
                }
              }

              /* CHECKSUM CHECKS */
              if (tgRecord.hasCheckSumFromHandleRecord) {
                /* there is a checksum in HandleRecord */

                if (tgObjectCheckumFromFile.equals(tgRecord.getCheckSumFromHandleRecord())) {
                  /* checksum in HandleRecord == checksum of File */

                  if (metadataChecksum != null) {
                    /* there is a checksum in Metadata */

                    if (metadataChecksum.equals(tgObjectCheckumFromFile)) {
                      /* all checksums are OK */
                      checksumFlag = 1;
                      checksumOpCode = CS_OC_OK;
                      checksumMessage = "OK-CS(1): ALL THREE CHECKSUMS ARE EQUAL";
                      this.logger.log(checksumMessage);
                    } else {
                      /* checksum in Metadata not OK */
                      checksumFlag = 0;
                      checksumOpCode = CS_OC_ERR_2000;
                      checksumMessage =
                          "WARNING-CS(2000): CHECKSUM IN METADATA DIFFERS FROM FILE AND HandleRecord";
                      this.logger.log(checksumMessage);
                    }
                  } else {
                    /* there is no checksum in Metadata */
                    checksumFlag = 0;
                    checksumOpCode = CS_OC_ERR_2010;
                    checksumMessage =
                        "WARNING-CS(2010): CHECKSUM IN METADATA IS NULL BUT HandleRecord and File EQUAL";
                    this.logger.log(checksumMessage);
                  }
                } else {
                  /* checksum in HandleRecord != checksum of File */

                  if (metadataChecksum != null) {
                    /* there is a checksum in Metadata */

                    if (metadataChecksum.equals(tgObjectCheckumFromFile)) {
                      /* checksum in Metadata == checksum of File */
                      checksumFlag = 0;
                      checksumOpCode = CS_OC_ERR_2020;
                      checksumMessage =
                          "WARNING-CS(2020): CHECKSUM IN HandleRecord Differs FROM FILE AND METADATA";
                      this.logger.log(checksumMessage);
                    } else {
                      checksumFlag = 0;
                      checksumOpCode = CS_OC_ERR_2030;
                      checksumMessage = "WARNING-CS(2030): ALL CHECKSUMS ARE DIFFERENT";
                      this.logger.log(checksumMessage);
                    }
                  } else {
                    /* there is no checksum in Metadata */
                    /* and checksum of file and HandleRecord not equal */
                    checksumFlag = 0;
                    checksumOpCode = CS_OC_ERR_2040;
                    checksumMessage =
                        "WARNING-CS(2040): CHECKSUM IN METADATA IS NULL AND HandleRecord != File EQUAL";
                    this.logger.log(checksumMessage);
                  }
                }
              } else {
                /* no checksum in HandleRecord */
                if (metadataChecksum != null) {
                  /* there is a checksum in Metadata */

                  if (metadataChecksum.equals(tgObjectCheckumFromFile)) {
                    /* checksum in Metadata == checksum of File */
                    checksumFlag = 0;
                    checksumOpCode = CS_OC_ERR_2050;
                    checksumMessage =
                        "WARNING-CS(2050): (NO CHECKSUM IN HandleRecord) CHECKSUM IS EQUAL FOR FILE AND METADATA";
                    this.logger.log(checksumMessage);
                  } else {
                    /* checksum in Metadata != checksum of File */
                    checksumFlag = 0;
                    checksumOpCode = CS_OC_ERR_2060;
                    checksumMessage =
                        "WARNING-CS(2060): (NO CHECKSUM IN HandleRecord) ALL CHECKSUMS ARE DIFFERENT";
                    this.logger.log(checksumMessage);
                  }
                } else {
                  /* there is no checksum in Metadata (and no checksum in HandleRecord) */
                  /* and checksum of file and HandleRecord not equal */

                  checksumFlag = 0;
                  checksumOpCode = CS_OC_ERR_2070;
                  checksumMessage =
                      "WARNING-CS(2070): (NO CHECKSUM IN HandleRecord) (NO CHECKSUM IN METADATA)";
                  this.logger.log(checksumMessage);
                }
              }

              this.logger.log("checksum comparison complete");
              this.logger.check_detailed(handle, tgRecord.getUri(), 1, EX1, filesizeFlag,
                  filesizeOpCode, checksumFlag, checksumOpCode);
            }
          } catch (IOException e) {
            e.printStackTrace();
          }
        } else {
          this.logger.noURI(handle);
        }
      } else {
        this.logger.noURL(handle);
      }
    } else {
      this.logger.noTgPID(handle);
    }
  }

  /**
   * @param values
   * @param handle
   */
  public void examineAndProcessFileSizeOnly(HandleValue[] values, String handle) {

    this.logger.log("examining and processing textgrid handle record: " + handle);

    TextGridRecord tgRecord = new TextGridRecord(this.logger);

    boolean isTextGridRelated = false;
    for (int i = 0; i < values.length; i++) {

      String theType = values[i].getTypeAsString();
      String theData = values[i].getDataAsString();

      if (theType.equals("URL")) {
        if (tgRecord.urlHasTextGridPattern(theData)) {
          isTextGridRelated = true;
          tgRecord.setUri(theData);
        } else {
          isTextGridRelated = false;
        }
      }
    }

    int filesizeFlag = -1;
    String filesizeOpCode = null;
    String filesizeMessage = null;

    if (isTextGridRelated) {
      if (tgRecord.hasUrl) {
        String objectPath = tgRecord.getObjectPath(this.textGridStoragePath);
        if (objectPath != null) {
          try {

            this.logger.log("reading file from: " + objectPath);

            File tgObject = new File(objectPath);
            int objectExists = 0;
            if (tgObject.exists() && !tgObject.isDirectory()) {

              this.logger.log("tg object exists:" + tgRecord.getUri());

              objectExists = 1;
            } else {
              this.logger.log("tg object is not existing");
              this.logger.check_detailed(handle, tgRecord.getUri(), 0, "", 0, "", 0, "");
            }

            if (objectExists == 1) {
              byte[] tgObjectBytes = Files.toByteArray(tgObject);

              this.logger.log("extracting filesize from metadata");

              FileSizeAndCheckum fsCs = TextGridMetaDataFilesizeAndChecksum(objectPath + ".meta");
              int metadataFilesize = fsCs.filesize;
              int sizeOnDisk = tgObjectBytes.length;

              /* FILESIZE CHECKS */
              if (metadataFilesize > -1) {
                /* there is a filesize in Metadata */

                if (metadataFilesize == sizeOnDisk) {
                  /* checksum in filesize == filesize of File */

                  filesizeFlag = 0;
                  filesizeOpCode = OK_FS_ONLY;
                  filesizeMessage = filesizeOpCode + ": FILESIZE IS EQUAL FOR FILE AND METADATA";
                  this.logger.log(filesizeMessage);
                } else {
                  /* filesize in Metadata != filesize of File */
                  filesizeFlag = 1;
                  filesizeOpCode = WARN_FS_ONLY_01;
                  filesizeMessage = filesizeOpCode + ": FILESIZE ARE DIFFERENT";
                  this.logger.log(filesizeMessage);
                }
              } else {
                /* there is no filesize in Metadata (and no filesize in HandleRecord) */
                /* and filesize of file and HandleRecord not equal */

                filesizeFlag = 2;
                filesizeOpCode = WARN_FS_ONLY_02;
                filesizeMessage = filesizeOpCode + ": NO FILESIZE IN METADATA";
                this.logger.log(filesizeMessage);
              }

              this.logger.log("filesize comparison complete");
              this.logger.check_filesize_only(handle, tgRecord.getUri(), filesizeOpCode,
                  metadataFilesize, sizeOnDisk);
            }
          } catch (IOException e) {
            e.printStackTrace();
          }
        } else {
          this.logger.noURI(handle);
        }
      } else {
        this.logger.noURL(handle);
      }
    } else {
      this.logger.noTgPID(handle);
    }
  }

  /**
   * <p>
   * This is method No2!
   * </p>
   * 
   * <p>
   * Checks if the Handle metadata of a Handle PID is consistent with TextGrid PID metadata rules.
   * </p>
   * 
   * @param values
   * @param handle
   */
  public void examineHandleRecord(HandleValue[] values, String handle) {

    String code = "";

    this.logger.log("examining handle record: " + handle);

    String blablubb = "PID" + ITEM_CHAR + handle + SEPA_CHAR;

    int cURL = 0;
    int cMetadata = 0;
    int cFilesize = 0;
    int cChecksum = 0;
    int cPubdate = 0;
    int cAuthors = 0;
    int cCreator = 0;
    int cHSAdmin = 0;

    for (int i = 0; i < values.length; i++) {

      String theType = values[i].getTypeAsString();
      String theData = values[i].getDataAsString();

      if (theType.equals("URL")) {
        blablubb += "DATA" + ITEM_CHAR + theData + SEPA_CHAR;
        if (theData.contains("textgridrep.org")) {
          cURL = 1;
        }
      }
      if (theType.equals("METADATA")) {
        blablubb += "METADATA" + ITEM_CHAR + theData + SEPA_CHAR;
        if (theData.contains("textgridrep.org") && theData.contains("metadata")) {
          cMetadata = 1;
        }
      }
      if (theType.equals("FILESIZE")) {
        blablubb += "FILESIZE" + ITEM_CHAR + theData + SEPA_CHAR;
        if (Long.parseLong(theData) > 0) {
          cFilesize = 1;
        }
      }
      if (theType.equals("CHECKSUM")) {
        blablubb += "CHECMSUM" + ITEM_CHAR + theData + SEPA_CHAR;
        if (theData.contains("md5:")) {
          cMetadata = 1;
        }
      }
      if (theType.equals("PUBDATE")) {
        blablubb += "PUBDATE" + ITEM_CHAR + theData + SEPA_CHAR;
        if (theData.matches("[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]")) {
          cPubdate = 1;
        }
      }
      if (theType.equals("AUTHORS")) {
        blablubb += "AUTHORS" + ITEM_CHAR + theData + SEPA_CHAR;
        if (!theData.isEmpty()) {
          cAuthors = 1;
        }
      }
      if (theType.equals("CREATOR")) {
        blablubb += "CREATOR" + ITEM_CHAR + theData + SEPA_CHAR;
        if (!theData.isEmpty()) {
          cCreator = 1;
        }
      }
      if (theType.equals("HS_ADMIN")) {
        blablubb += "HS_ADMIN" + ITEM_CHAR + theData;
        if (true) {
          cHSAdmin = 1;
        }
      }
    }

    code = String.valueOf(cURL) + String.valueOf(cMetadata) + String.valueOf(cFilesize)
        + String.valueOf(cChecksum) + String.valueOf(cPubdate) + String.valueOf(cAuthors)
        + String.valueOf(cCreator) + String.valueOf(cHSAdmin);
    long longFromBin = Long.parseLong(code, 2);

    if (this.categories.containsKey(longFromBin)) {
      long count = this.categories.get(longFromBin) + 1;
      this.categories.replace(longFromBin, count);
    } else {
      this.categories.put(longFromBin, 0l);
    }

    System.out.println(blablubb + SEPA_CHAR + "CODE" + ITEM_CHAR + code + SEPA_CHAR + "INT"
        + ITEM_CHAR + longFromBin);
    System.out.println(this.categories);
  }

  /**
   * @param TextGridObjectsBytes
   * @param tgUri
   * @return
   */
  public String calculateMD5Checksum(byte[] TextGridObjectsBytes, String tgUri) {

    String tgObjectChecksum = null;
    MessageDigest md;
    try {
      md = MessageDigest.getInstance("MD5");
      md.update(TextGridObjectsBytes);

      // byte byteData[] = md.digest();
      tgObjectChecksum = new String(Hex.encodeHex(md.digest()));
      this.logger.log("calculated checkusm of the tg object: " + tgUri);

    } catch (NoSuchAlgorithmException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return tgObjectChecksum;
  }

  /**
   * @param metadataFilePath
   * @return
   */
  public FileSizeAndCheckum TextGridMetaDataFilesizeAndChecksum(String metadataFilePath) {

    String checksum = null;
    String filesize = null;

    try {
      File tgMetaDataFile = new File(metadataFilePath);
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(tgMetaDataFile);
      doc.getDocumentElement().normalize();

      NodeList tgMetaListForFileSize = doc.getElementsByTagName(FILESIZEFIELD_EXTENT);
      if (tgMetaListForFileSize.getLength() > 0) {
        Node nodeFsize = tgMetaListForFileSize.item(0);
        Element fzElem = (Element) nodeFsize;
        filesize = fzElem.getTextContent();
      }

      NodeList tgMetaList = doc.getElementsByTagName(CHECKSUMFIELD_FIXITY);

      for (int tgMeta = 0; tgMeta < tgMetaList.getLength(); tgMeta++) {
        Node tgMetaNode = tgMetaList.item(tgMeta);

        if (tgMetaNode.getNodeType() == Node.ELEMENT_NODE) {
          Element eElement = (Element) tgMetaNode;
          String metadataCheckum =
              eElement.getElementsByTagName(CHECKSUMFIELD_CHECKSUM).item(0).getTextContent();
          checksum = metadataCheckum;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    FileSizeAndCheckum fsCs = new FileSizeAndCheckum();
    fsCs.checksum = checksum;
    if (filesize != null) {
      fsCs.filesize = Integer.valueOf(filesize);
    }

    return fsCs;
  }

  /**
   *
   */
  public class FileSizeAndCheckum {
    String checksum = null;
    int filesize = -1;
  }

  /**
   * @return
   */
  public HashMap<Long, Long> getCategories() {
    return this.categories;
  }

}
