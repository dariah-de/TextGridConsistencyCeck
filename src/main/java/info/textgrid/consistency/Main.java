package info.textgrid.consistency;

/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/*
 * TextGrid Repository Consistency Check
 */
/* Consistency Checker: verifies if the published TextGrid object */
/* has been altered. For that it first queries the HandleRecord to */
/* determine (1) object URL (2) object size (3) checksum of obejct */
/*                 				        								*/
/* Author: Fatih Berber */
/* Email: fatih.berber@gwdg.de */
/* ORCID: orcid.org/0000-0001-5507-4048 */
/*																		*/
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 */
public class Main {

  private static final String GET_TG_HANDLES = "get_tg_handles";
  private static final String CHECK_TG_OBJECTS = "check_tg_objects";
  private static final String CHECK_TG_HANDLES = "check_tg_handles";
  private static final String CHECK_FILESIZE_ONLY_NO_HANDLE = "check_filesizes_no_handle";
  private static final String GET_TG_HANDLES_FROM_DISK = "get_tg_handles_from_disk";

  /**
   * @param args
   */
  public static void main(String[] args) {

    String config_filename = args[0];
    Config.createInstance(config_filename);

    Config.getInstance();
    String loggingPath = Config.loggingPath;
    Config.getInstance();
    String textgridStoragePath = Config.textgridStoragePath;
    Config.getInstance();
    String textgridHandlesListPath = Config.textgridHandlesListPath;
    Config.getInstance();
    String pidPrefix = Config.pidPrefix;
    Config.getInstance();
    String epicUser = Config.epicUser;
    Config.getInstance();
    String epicUserPw = Config.epicUserPw;
    Config.getInstance();
    String epicHost = Config.epicHost;
    Config.getInstance();
    String runType = Config.runType;
    Config.getInstance();
    String epicLimit = Config.epicLimit;

    Logger logger = new Logger(loggingPath);

    HandleMethods hs = new HandleMethods(logger);
    HttpMethods httpMethods =
        new HttpMethods(logger, pidPrefix, epicUser, epicUserPw, epicHost, epicLimit);

    logger.log("----------  starting up textgrid consistency check  ---------------------------");
    logger.log("       ***  run type: " + runType + "  ***");

    try {
      /* runType == get_tg_handles => get PID-List from ePIC API and store to file */
      if (runType.equals(GET_TG_HANDLES)) {

        httpMethods.provisionTextGridHandlesList();

        logger.log("store handles list to file");

        httpMethods.storeTextGridHandlesListToFile(textgridHandlesListPath);
      }

      /* runType == check_tg_objects => read the PID-List input file and do the scrutiny */
      else if (runType.equals(CHECK_TG_OBJECTS)) {

        TextGridHandlesList textgridHandlesList =
            new TextGridHandlesList(logger, textgridHandlesListPath);
        textgridHandlesList.readSimpleHandleList();

        ArrayList<String> toBeCheckedHandles = textgridHandlesList.getLegacyHandles();

        File alreadyCheckedList = new File(logger.checkPath);
        // File alreadyCheckedListNoTgPID = new File(logger.noTextGridPID);
        // File alreadyCheckedListNoTgUri = new File(logger.noTextGridURI);
        // File alreadyCheckedListNoTgURL = new File(logger.noTextGridURL);

        if (alreadyCheckedList.exists() && !alreadyCheckedList.isDirectory()) {

          logger.log("there is an already checked handles list");

          textgridHandlesList.readAlreadyCheckedList(logger.checkPath);

          logger.log("getting only remaining handles list");

          toBeCheckedHandles = textgridHandlesList.getRemainingHandleList();
        }

        logger.log("reading of legacy handles done, checking textgrid objects now");

        TextGridHandlesReader checkTGHandle =
            new TextGridHandlesReader(logger, toBeCheckedHandles, hs, textgridStoragePath, false);
        checkTGHandle.run();
      }

      /*
       * runType == check_tg_handles => just checks all the given handles for completeness and
       * consistency.
       */
      else if (runType.equals(CHECK_TG_HANDLES)) {

        // Read handle list.
        TextGridHandlesList textgridHandlesList =
            new TextGridHandlesList(logger, textgridHandlesListPath);
        textgridHandlesList.readSimpleHandleList();
        ArrayList<String> toBeCheckedHandles = textgridHandlesList.getLegacyHandles();

        TextGridHandlesReader checkTGHandle =
            new TextGridHandlesReader(logger, toBeCheckedHandles, hs);
        checkTGHandle.run();

        System.out.println(checkTGHandle.getCategories());
      }

      /*
       * runType == check_filesizes_no_handle => just checks all the given handles for file sizes in
       * filesystem and metadata.
       */
      else if (runType.equals(CHECK_FILESIZE_ONLY_NO_HANDLE)) {

        // TODO Does not run properly yet, as it seems...

        TextGridHandlesList textgridHandlesList =
            new TextGridHandlesList(logger, textgridHandlesListPath);
        textgridHandlesList.readSimpleHandleList();

        ArrayList<String> toBeCheckedHandles = textgridHandlesList.getLegacyHandles();

        File alreadyCheckedList = new File(logger.checkPath);
        // File alreadyCheckedListNoTgPID = new File(logger.noTextGridPID);
        // File alreadyCheckedListNoTgUri = new File(logger.noTextGridURI);
        // File alreadyCheckedListNoTgURL = new File(logger.noTextGridURL);

        if (alreadyCheckedList.exists() && !alreadyCheckedList.isDirectory()) {

          logger.log("there is an already checked handles list");

          textgridHandlesList.readAlreadyCheckedList(logger.checkPath);

          logger.log("getting only remaining handles list");

          toBeCheckedHandles = textgridHandlesList.getRemainingHandleList();
        }

        logger.log("reading of legacy handles done, checking textgrid objects now");

        // Check file sizes in metadata and on disk only!
        TextGridHandlesReader checkTGHandle =
            new TextGridHandlesReader(logger, toBeCheckedHandles, hs, textgridStoragePath, true);
        checkTGHandle.run();
      }

      /*
       * runType == get_tg_handles_from_disk => get PID-List from pairtree file system
       */
      else if (runType.equals(GET_TG_HANDLES_FROM_DISK)) {

        File storagePath = new File(textgridStoragePath);
        logger.log("getting textgrid handle list from pairtree file system: "
            + storagePath.getCanonicalPath());

        File pidFile = new File(textgridHandlesListPath);
        logger.log("storing handle list to file: " + pidFile.getCanonicalPath());

        try (FileWriter writePids = new FileWriter(pidFile)) {
          httpMethods.provisionAndStoreTextGridHandlesListFromDisk(storagePath, writePids);
          writePids.close();
        }
      }

      logger.log("       ***  " + runType + " job complete  ***");

    } catch (IOException e) {
      logger.log(e.getMessage());
      logger.log("----------  error  ------------------------------------------------------------");
      System.exit(1);
    }

    logger.log("----------  all done  ---------------------------------------------------------");
  }

}
