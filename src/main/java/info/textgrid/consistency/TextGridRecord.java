package info.textgrid.consistency;

/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/*																		*/
/* TextGridRecord for Consistency Checker */
/* Author: Fatih Berber */
/* Email: fatih.berber@gwdg.de */
/* ORCID: orcid.org/0000-0001-5507-4048 */
/*																		*/
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */

import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.validator.routines.UrlValidator;

/**
 *
 */
public class TextGridRecord {

  public boolean hasUri = false;
  public boolean hasUrl = false;
  public boolean hasFileSizeFromHandleRecord = false;
  public boolean hasCheckSumFromHandleRecord = false;

  private String uri;
  private String url;
  private String fileSize;
  private String checkSum;
  private Logger logger;

  /**
   * @param logger
   */
  public TextGridRecord(Logger logger) {
    this.logger = logger;
  }

  /**
   * @param uri
   */
  public void setUri(String uri) {

    UrlValidator urlValidator = new UrlValidator();

    if (urlValidator.isValid(uri)) {
      try {
        URL url = new URL(uri);
        String path = url.getPath();
        // this.uri = path.substring(1);


        String[] pathAndUri = path.split("/");
        this.uri = pathAndUri[pathAndUri.length - 1];

        this.hasUri = true;
        this.hasUrl = true;

        this.logger.log("HAS_URL_HAS_URI-" + uri);

      } catch (MalformedURLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    } else {
      this.uri = uri;
      this.hasUri = true;
    }
  }

  /**
   * @param theUrl
   * @return
   */
  public boolean urlHasTextGridPattern(String theUrl) {
    String pattern = "textgrid";
    Pattern r = Pattern.compile(pattern);
    Matcher m = r.matcher(theUrl);
    return m.find();
  }

  /**
   * @param fileSize
   */
  public void setFileSizeFromHandleRecord(String fileSize) {
    this.fileSize = fileSize;
    this.hasFileSizeFromHandleRecord = true;
  }

  /**
   * @param checkSum
   */
  public void setCheckSumFromHandleRecord(String checkSum) {

    int colonCheck = checkSum.indexOf(":");
    if (colonCheck > -1) {
      String[] splt_cs = checkSum.split(":");

      this.checkSum = splt_cs[1];
      this.hasCheckSumFromHandleRecord = true;
    } else {
      this.hasCheckSumFromHandleRecord = false;
    }
  }

  /**
   * @return
   */
  public String getUrl() {
    return this.url;
  }

  /**
   * @return
   */
  public String getUri() {
    return this.uri;
  }

  /**
   * @return
   */
  public int getFileSizeFromHandleRecord() {
    return Integer.valueOf(this.fileSize);
  }

  /**
   * @return
   */
  public String getCheckSumFromHandleRecord() {
    return this.checkSum;
  }

  /**
   * @param basePath
   * @return
   */
  public String getObjectPath(String basePath) {

    String pattern = "textgrid:(.*)";

    // Create a Pattern object
    Pattern r = Pattern.compile(pattern);

    if (this.hasUri && this.hasUrl) {
      // Now create matcher object.
      Matcher m = r.matcher(this.uri);
      if (m.find()) {
        String textgridUri = m.group(1);
        int pointIdx = textgridUri.indexOf(".");
        if (pointIdx > -1) {

          String textgridUriNoPoint =
              textgridUri.substring(0, pointIdx) + "," + textgridUri.substring(pointIdx + 1);

          StringBuilder objectName = new StringBuilder("textgrid+");
          StringBuilder pathBuilder = new StringBuilder(basePath + "/+");

          pathBuilder.append(textgridUriNoPoint.substring(0, 1) + "/");
          objectName.append(textgridUriNoPoint.substring(0, 1));

          String textgridUriNoPointRest = textgridUriNoPoint.substring(1);

          String[] pairs = textgridUriNoPointRest.split("(?<=\\G.{2})");

          for (int j = 0; j < pairs.length; j++) {
            pathBuilder.append(pairs[j] + "/");
            objectName.append(pairs[j]);
          }

          return pathBuilder.toString() + objectName.toString();

        } else {
          return null;
        }
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

}
