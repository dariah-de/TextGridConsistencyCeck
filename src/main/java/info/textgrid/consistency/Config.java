package info.textgrid.consistency;

/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/*																		*/
/* Config for Integrity Checker */
/* Author: Fatih Berber */
/* Email: fatih.berber@gwdg.de */
/* ORCID: orcid.org/0000-0001-5507-4048 */
/*																		*/
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 */
public class Config {

  private static Config instance = null;

  public static String runType;
  public static String loggingPath;
  public static String textgridStoragePath;
  public static String textgridHandlesListPath;
  public static String pidPrefix;
  public static String epicUser;
  public static String epicUserPw;
  public static String epicHost;
  public static String epicLimit;

  /**
   * @param configFilename
   */
  public static void createInstance(String configFilename) {
    instance = new Config(configFilename);
  }

  /**
   * @return
   */
  public static Config getInstance() {
    return instance;
  }

  /**
   * @param config_filename
   */
  private Config(String config_filename) {

    Properties prop = new Properties();
    InputStream input = null;

    try {
      input = new FileInputStream(config_filename);

      // load a properties file
      prop.load(input);

      Config.runType = prop.getProperty("run_type");
      Config.loggingPath = prop.getProperty("logging_path");
      Config.textgridStoragePath = prop.getProperty("textgrid_storage_path");
      Config.textgridHandlesListPath = prop.getProperty("textgrid_handles_list_path");
      Config.pidPrefix = prop.getProperty("pid_prefix");
      Config.epicUser = prop.getProperty("epic_user");
      Config.epicUserPw = prop.getProperty("epic_user_pw");
      Config.epicHost = prop.getProperty("epic_host");
      Config.epicLimit = prop.getProperty("epic_limit");

    } catch (IOException ex) {
      ex.printStackTrace();
    } finally {
      if (input != null) {
        try {
          input.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

}
