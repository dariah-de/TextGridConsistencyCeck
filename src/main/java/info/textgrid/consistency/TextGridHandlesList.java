package info.textgrid.consistency;

/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/*																		*/
/* HandleList for LegacyHandlesMigrator */
/* Author: Fatih Berber */
/* Email: fatih.berber@gwdg.de */
/* ORCID: orcid.org/0000-0001-5507-4048 */
/*																		*/
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 *
 */
public class TextGridHandlesList {

  private Logger logger;
  private String simpleHandleListPath;
  private ArrayList<String> legacyHandles = new ArrayList<String>();
  private ArrayList<String> alreadyCheckedHandles = new ArrayList<String>();

  /**
   * @param logger
   * @param handleListPath
   */
  public TextGridHandlesList(Logger logger, String handleListPath) {
    this.logger = logger;
    this.simpleHandleListPath = handleListPath;
  }

  /**
   * @return
   */
  /**
   * @return
   */
  public ArrayList<String> getLegacyHandles() {
    return this.legacyHandles;
  }

  /**
   * @param threadCount
   * @return
   */
  public ArrayList<ArrayList<String>> getLegacyHandles(int threadCount) {

    ArrayList<ArrayList<String>> listsPerThread = new ArrayList<ArrayList<String>>();
    int listLength = this.legacyHandles.size();
    if (threadCount > listLength) {

      this.logger.log("ERROR: threadCount > ListSize");

      return null;

    } else {

      this.logger.log("LIST-SLICING: threadCount=" + threadCount);

      int perThreadHandlesCount = listLength / threadCount;
      int totalCount_first_part = perThreadHandlesCount * threadCount;
      int offset = totalCount_first_part;
      int remaining = listLength - totalCount_first_part;

      ArrayList<String> perThreadList = new ArrayList<String>();

      this.logger.log("LIST-SLICING: totalCount=" + listLength);
      this.logger.log("LIST-SLICING: totalCount(withoutRest)=" + totalCount_first_part);
      this.logger.log("LIST-SLICING: perThreadHandlesCount=" + perThreadHandlesCount);
      this.logger.log("LIST-SLICING: remaining=" + remaining);

      for (int j = 0; j < totalCount_first_part; j++) {
        String hdl = this.legacyHandles.get(j);
        if (perThreadList.size() < perThreadHandlesCount) {
          perThreadList.add(hdl);
        } else {
          listsPerThread.add(perThreadList);
          perThreadList = new ArrayList<String>();
          perThreadList.add(hdl);
        }
      }

      listsPerThread.add(perThreadList);
      ArrayList<String> perThreadList_Frist = listsPerThread.get(0);
      for (int k = offset; k < listLength; k++) {
        String hdl = this.legacyHandles.get(k);
        perThreadList_Frist.add(hdl);
      }

      this.logger.log("PER THREAD LIST SIZES:");

      for (int i = 0; i < listsPerThread.size(); i++) {
        this.logger.log("Thread-Id=" + i + " ListSize=" + listsPerThread.get(i).size());
      }

      return listsPerThread;
    }
  }

  /**
   * @param threadCount
   * @param dummyPrefix
   * @return
   */
  public ArrayList<ArrayList<String>> getLegacyHandles(int threadCount, String dummyPrefix) {

    ArrayList<ArrayList<String>> listsPerThread = new ArrayList<ArrayList<String>>();
    int listLength = this.legacyHandles.size();
    int perThreadHandlesCount = listLength / threadCount;
    ArrayList<String> perThreadList = new ArrayList<String>();

    for (int i = 0; i < this.legacyHandles.size(); i++) {
      String hdl = this.legacyHandles.get(i);
      String[] splt_hdl = hdl.split("/");
      String dmmyHdl = dummyPrefix + "/" + splt_hdl[1];

      perThreadList.add(dmmyHdl);

      if ((i + 1) <= perThreadHandlesCount * threadCount) {
        if (perThreadList.size() == perThreadHandlesCount) {
          listsPerThread.add(perThreadList);
          if (listsPerThread.size() < threadCount) {
            perThreadList = new ArrayList<String>();
          }
        }
      }
    }

    listsPerThread.add(perThreadList);

    return listsPerThread;
  }

  /**
   * @return
   */
  public ArrayList<String> getRemainingHandleList() {

    this.logger.log("exctracting already checked handles");

    Set<String> handleHash = new HashSet<String>();
    handleHash.addAll(this.legacyHandles);
    handleHash.addAll(this.alreadyCheckedHandles);

    ArrayList<String> remainingHandles = new ArrayList<String>();
    remainingHandles.addAll(handleHash);

    return remainingHandles;
  }

  /**
   * @param alreadyCheckedList
   */
  public void readAlreadyCheckedList(String alreadyCheckedList) {

    this.logger.log("reading already cecked list = " + alreadyCheckedList);

    FileInputStream fstream;
    try {
      fstream = new FileInputStream(alreadyCheckedList);
      BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

      String oldHandle;

      int cc = 0;
      // Read File Line By Line
      while ((oldHandle = br.readLine()) != null) {
        // Print the content on the console

        String[] splt_line = oldHandle.split(",");
        String hdl = splt_line[0];
        this.alreadyCheckedHandles.add(hdl);

        cc++;
      }

      this.logger.log("reading already checked handle list done!");
      this.logger.log("already check handles count = " + cc);

      // Close the input stream
      br.close();
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  /**
   * 
   */
  public void readSimpleHandleList() {

    this.logger.log("reading simple handle list = " + this.simpleHandleListPath);

    FileInputStream fstream;
    try {
      fstream = new FileInputStream(this.simpleHandleListPath);
      BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

      String oldHandle;
      int cc = 0;
      // Read File Line By Line
      while ((oldHandle = br.readLine()) != null) {
        // Print the content on the console
        this.legacyHandles.add(oldHandle);
        cc++;
      }

      this.logger.log("reading simple handle lists done!");
      this.logger.log("handles count = " + cc);

      // Close the input stream
      br.close();
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

}
