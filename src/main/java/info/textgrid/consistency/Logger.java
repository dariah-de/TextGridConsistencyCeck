package info.textgrid.consistency;

/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/*																		*/
/* Logger for Consistency Checker */
/* Author: Fatih Berber */
/* Email: fatih.berber@gwdg.de */
/* ORCID: orcid.org/0000-0001-5507-4048 */
/*																		*/
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 */
public class Logger {

  private String fileName;
  public String checkPath;
  private String filesizeOnlyPath;
  private String errorPath;
  public String noTextGridURL;
  public String noTextGridURI;
  public String noTextGridPID;

  /**
   * @param loggerOutputBasePath
   */
  public Logger(String loggerOutputBasePath) {

    this.fileName = loggerOutputBasePath;
    this.checkPath = loggerOutputBasePath + "_checklist";
    this.filesizeOnlyPath = loggerOutputBasePath + "_filesize_only";
    this.noTextGridURL = loggerOutputBasePath + "_no_textgrid_url";
    this.noTextGridURI = loggerOutputBasePath + "_no_textgrid_uri";
    this.noTextGridPID = loggerOutputBasePath + "_no_textgrid_pid";
    this.errorPath = loggerOutputBasePath + "_errorlog";
  }

  /**
   * @param handle
   */
  public synchronized void noURI(String handle) {

    FileWriter output = null;
    try {
      output = new FileWriter(this.noTextGridURI, true);
      BufferedWriter writer = new BufferedWriter(output);

      String out_msg = handle;

      writer.write(out_msg + "\n");
      writer.close();

    } catch (Exception e) {
      System.out.println("filewritererror");
      throw new RuntimeException(e);
    } finally {
      if (output != null) {
        try {
          output.close();
        } catch (IOException e) {
          // Ignore issues during closing
        }
      }
    }
  }

  /**
   * @param handle
   */
  public synchronized void noTgPID(String handle) {

    FileWriter output = null;
    try {
      output = new FileWriter(this.noTextGridPID, true);
      BufferedWriter writer = new BufferedWriter(output);

      String out_msg = handle;

      writer.write(out_msg + "\n");
      writer.close();

    } catch (Exception e) {
      System.out.println("filewritererror");
      throw new RuntimeException(e);
    } finally {
      if (output != null) {
        try {
          output.close();
        } catch (IOException e) {
          // Ignore issues during closing
        }
      }
    }
  }

  /**
   * @param handle
   */
  public synchronized void noURL(String handle) {

    FileWriter output = null;
    try {
      output = new FileWriter(this.noTextGridURL, true);
      BufferedWriter writer = new BufferedWriter(output);

      String out_msg = handle;

      writer.write(out_msg + "\n");
      writer.close();

    } catch (Exception e) {
      System.out.println("filewritererror");
      throw new RuntimeException(e);
    } finally {
      if (output != null) {
        try {
          output.close();
        } catch (IOException e) {
          // Ignore issues during closing
        }
      }
    }
  }

  /**
   * @param handle
   * @param tgURI
   * @param exFlag
   * @param exCode
   * @param fsFlag
   * @param fsCode
   * @param csFlag
   * @param csCode
   */
  public synchronized void check_detailed(String handle, String tgURI, int exFlag, String exCode,
      int fsFlag, String fsCode, int csFlag, String csCode) {

    FileWriter output = null;
    try {
      output = new FileWriter(this.checkPath, true);
      BufferedWriter writer = new BufferedWriter(output);

      String out_msg = handle + "," + tgURI + "," + String.valueOf(exFlag) + "," + exCode + ","
          + String.valueOf(fsFlag) + "," + fsCode + "," + String.valueOf(csFlag) + "," + csCode;

      writer.write(out_msg + "\n");
      writer.close();

    } catch (Exception e) {
      System.out.println("filewritererror");
      throw new RuntimeException(e);
    } finally {
      if (output != null) {
        try {
          output.close();
        } catch (IOException e) {
          // Ignore issues during closing
        }
      }
    }
  }

  /**
   * @param handle
   * @param tgURI
   * @param opcode
   * @param filesizeMD
   * @param filesizeDISK
   */
  public synchronized void check_filesize_only(String handle, String tgURI, String opcode,
      int filesizeMD, int filesizeDISK) {

    FileWriter output = null;
    try {
      output = new FileWriter(this.filesizeOnlyPath, true);
      BufferedWriter writer = new BufferedWriter(output);

      String out_msg = handle + "," + tgURI + "," + opcode + ",MD:"
          + String.valueOf(filesizeMD + ",DI:" + String.valueOf(filesizeDISK));

      writer.write(out_msg + "\n");
      writer.close();

    } catch (Exception e) {
      System.out.println("filewritererror");
      throw new RuntimeException(e);
    } finally {
      if (output != null) {
        try {
          output.close();
        } catch (IOException e) {
          // Ignore issues during closing
        }
      }
    }
  }

  /**
   * @param handle
   * @param tgURI
   * @param opFlag
   * @param opCode
   * @param msg
   */
  public synchronized void check(String handle, String tgURI, int opFlag, String opCode,
      String msg) {

    FileWriter output = null;
    try {
      output = new FileWriter(this.checkPath, true);
      BufferedWriter writer = new BufferedWriter(output);

      String out_msg =
          handle + "," + tgURI + "," + String.valueOf(opFlag) + "," + opCode + "," + msg;

      writer.write(out_msg + "\n");
      writer.close();

    } catch (Exception e) {
      System.out.println("filewritererror");
      throw new RuntimeException(e);
    } finally {
      if (output != null) {
        try {
          output.close();
        } catch (IOException e) {
          // Ignore issues during closing
        }
      }
    }
  }

  /**
   * @param s
   */
  public synchronized void errorlog(String s) {

    FileWriter output = null;
    try {
      output = new FileWriter(this.errorPath, true);
      BufferedWriter writer = new BufferedWriter(output);


      writer.write(s + "\n");
      writer.close();

    } catch (Exception e) {
      System.out.println("filewritererror");
      throw new RuntimeException(e);
    } finally {
      if (output != null) {
        try {
          output.close();
        } catch (IOException e) {
          // Ignore issues during closing
        }
      }
    }
  }

  /**
   * @param s
   */
  public synchronized void log(String s) {

    FileWriter output = null;
    try {
      output = new FileWriter(this.fileName, true);
      BufferedWriter writer = new BufferedWriter(output);


      writer.write(s + "\n");
      writer.close();

    } catch (Exception e) {
      System.out.println("filewritererror");
      throw new RuntimeException(e);
    } finally {
      if (output != null) {
        try {
          output.close();
        } catch (IOException e) {
          // Ignore issues during closing
        }
      }
    }
  }

}
