package info.textgrid.consistency;

/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/*																		*/
/* HandleMethods for Consistency Checker */
/* Author: Fatih Berber */
/* Email: fatih.berber@gwdg.de */
/* ORCID: orcid.org/0000-0001-5507-4048 */
/*																		*/
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */
/* ###################################################################### */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.PrivateKey;
import net.handle.hdllib.AbstractResponse;
import net.handle.hdllib.AuthenticationInfo;
import net.handle.hdllib.HandleException;
import net.handle.hdllib.HandleResolver;
import net.handle.hdllib.HandleValue;
import net.handle.hdllib.Interface;
import net.handle.hdllib.PublicKeyAuthenticationInfo;
import net.handle.hdllib.ResolutionRequest;
import net.handle.hdllib.ResolutionResponse;
import net.handle.hdllib.SecretKeyAuthenticationInfo;
import net.handle.hdllib.Util;

/**
 *
 */
public class HandleMethods {

  private HandleResolver resolver = null;
  private AuthenticationInfo authInfo;
  private Logger logger = null;
  private String ipAddress = null;
  private int portNumber = 0;

  public void refreshResolver() {
    this.resolver = new HandleResolver();
    // resolver.traceMessages = true;
    int[] prefProtocols = {Interface.SP_HDL_TCP};
    this.resolver.setPreferredProtocols(prefProtocols);
  }

  /**
   * @param logger
   */
  public HandleMethods(Logger logger) {
    this.logger = logger;
    refreshResolver();
  }

  /**
   * @param logger
   * @param prefix
   * @param index
   * @param privateKeyFilePath
   */
  public HandleMethods(Logger logger, String prefix, int index, String privateKeyFilePath) {
    this.logger = logger;
    this.authInfo = getAuthInfoPub(prefix, index, privateKeyFilePath);
    refreshResolver();
  }

  /**
   * @param logger
   * @param prefix
   * @param index
   * @param privateKeyFilePath
   * @param ipAddress
   * @param portNumber
   */
  public HandleMethods(Logger logger, String prefix, int index, String privateKeyFilePath,
      String ipAddress, int portNumber) {
    this.logger = logger;
    this.authInfo = getAuthInfoPub(prefix, index, privateKeyFilePath);
    this.ipAddress = ipAddress;
    this.portNumber = portNumber;
    refreshResolver();
  }

  /**
   * @param handle
   * @return
   */
  public HandleValue[] resolveHandleSpecial(String handle) {

    HandleValue[] resolution_values = null;
    ResolutionRequest resoReq =
        new ResolutionRequest(Util.encodeString(handle), null, null, this.authInfo);
    resoReq.ignoreRestrictedValues = false;
    AbstractResponse resp = null;

    try {
      InetAddress addr = InetAddress.getByName(this.ipAddress);
      resp = this.resolver.sendHdlTcpRequest(resoReq, addr, this.portNumber);
    } catch (HandleException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (UnknownHostException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    if (resp.responseCode == 1) {
      ResolutionResponse resResp = (ResolutionResponse) resp;
      try {
        resolution_values = resResp.getHandleValues();
      } catch (HandleException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    return resolution_values;
  }

  /**
   * @param handle
   * @return
   */
  public HandleValue[] readHandle(String handle) {

    this.logger.log("resolving handle: " + handle);

    ResolutionRequest readReq =
        new ResolutionRequest(Util.encodeString(handle), null, null, this.authInfo);
    HandleValue[] theValues = null;

    AbstractResponse response = null;
    int resCode = -1;

    try {
      response = this.resolver.processRequest(readReq);
      resCode = response.responseCode;

      if (resCode == 1) {
        ResolutionResponse reso_resp = (ResolutionResponse) response;
        theValues = reso_resp.getHandleValues();
      }

    } catch (HandleException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return theValues;
  }

  /**
   * @param hs_seckey
   * @param userHandle
   * @param index
   * @return
   */
  private static AuthenticationInfo getAuthInfoSec(String hs_seckey, String userHandle, int index) {

    AuthenticationInfo auth = null;

    try {
      auth = new SecretKeyAuthenticationInfo(Util.encodeString(userHandle), index,
          Util.encodeString(hs_seckey), true);
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return auth;
  }


  /**
   * @param prefix
   * @param index
   * @param privateKeyFilePath
   * @return
   */
  private AuthenticationInfo getAuthInfoPub(String prefix, int index, String privateKeyFilePath) {

    this.logger.log("handle methods: getting pubauthinfo for " + prefix);

    PublicKeyAuthenticationInfo pubkeyAuthInfo = null;
    String handleStr = "0.NA/" + prefix;
    String passphrase = "";
    File keyFile = new File(privateKeyFilePath);

    try {
      byte[] rawKey = new byte[(int) keyFile.length()];
      InputStream in = new FileInputStream(keyFile);
      int n = 0;
      int r = 0;
      while (n < rawKey.length && (r = in.read(rawKey, n, rawKey.length - n)) > 0)
        n += r;
      in.close();
      byte[] keyBytes = passphrase == null ? Util.decrypt(rawKey, new byte[0])
          : Util.decrypt(rawKey, Util.encodeString(passphrase));

      PrivateKey privateKey = Util.getPrivateKeyFromBytes(keyBytes, 0);
      this.logger.log("handle methods: got the private key for " + prefix);
      pubkeyAuthInfo = new PublicKeyAuthenticationInfo(Util.encodeString(handleStr),
          index, privateKey);
      this.logger.log("handle methods: building the authinfo for " + prefix);
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return pubkeyAuthInfo;
  }

}
