# TextGrid Repository Consistency Check

## BUILD JAR

* mvn clean package shade:shade


## AUTHORS

* fatih.berber@gwdg.de
* funk@sub.uni-goettingen.de


## USAGE

1. first run configuration "run_type=get_tg_handles" in config file to assemble all PIDs from Handle Service (put in file {textgrid_handles_list_path}).
    1. you can run "run_type=check_tg_handles" to just check PIDs for consistency.
    2. then run configuration "run_type=check_tg_objects" to start checking all PIDs (pass 1), TextGrid objects including data and metadata (size and checksums).
3. again run configuration "run_type=check_tg_objects" (pass 2) ??

	
## OUTPUT FILES

    {logging_path}                  --> general log file
    {logging_path}_no_textgrid_pid  --> is not a TextGrid PID at all
    {logging_path}_no_textgrid_uri  --> no valid TextGRID URI in HandleRecord
    {logging_path}_no_textgrid_url  --> not valid URL in HandleRecord
    {logging_path}_checklist        --> checklist of checked TextGrid PIDs with status codes


## CHECKLIST STATUS CODES (run_type=check_tg_objects)

    "FS-1"          --> all filesizes are fine
    "FS-2"          --> ? (--fu)
    "FS-3000"       --> filesize of file == filesize HandleRecord but filesize in Metadata differs
    "FS-3010"       --> filesize of file == filesize HandleRecord but no filesize in Metadata"
    "FS-3020"       --> filesize of file != filesize HandleRecord but filesize Metadata == filesize File
    "FS-3030"       --> filesize of file != filesize HandleRecord and filesize Metadata != filesize File (all different)
    "FS-3040"       --> filesize of file != filesize HandleRecord and filesize Metadata == NULL
    "FS-3050"       --> filesize of file == filesize Metadata but no filesize in HandleRecord
    "FS-3060"       --> filesize of file != filesize Metadata and no filesize in HandleRecord
    "FS-3070"       --> no filesize in Metadata and no filesize in HandleRecord

    "CS-1"          --> all checksums are fine
    "CS-2"          --> ? (--fu)
    "CS-2000"       --> checksum of file == checksum HandleRecord but checksum in Metadata differs
    "CS-2010"       --> checksum of file == checksum HandleRecord but no checksum in Metadata
    "CS-2020"       --> checksum of file != checksum HandleRecord but checksum Metadata == checksum File
    "CS-2030"       --> checksum of file != checksum HandleRecord and checksum Metadata != checksum File (all different)
    "CS-2040"       --> checksum of file != checksum HandleRecord and checksum Metadata == NULL
    "CS-2050"       --> checksum of file == checksum Metadata but no checksum in HandleRecord
    "CS-2060"       --> checksum of file != checksum Metadata and no checksum in HandleRecord
    "CS-2070"       --> no checksum in Metadata and no checksum in HandleRecord


## HANDLE METADATA REQUIREMENTS ON A TEXTGRID PID (run_type=check_tg_handles)

### Example record
 
1	URL               http://textgridrep.org/textgrid:165hp.0
2	METADATA_URL      http://textgridrep.org/textgrid:165hp.0/metadata
3	CHECKSUM          md5:06d7f58775d12f83b61f640f1f5323f5
4	PUBDATE	          2016-11-11
5	AUTHORS           TextGrid
6	CREATOR           PID Service pid-webapp-4.8.0-BETA.201609051655
7	INST	          1000
100	HS_ADMIN		  ? (siehe unten) ?

### Required are

1. HTTPS URL and content "textgridrep.org"
2. HTTPS METADATA URL and content "textgridrep.org"
3. CHECKSUM exists and content "md5:{checksum}"
4. PUBDATE exists and format is "2019-10-02"
5. AUTHORS exists and is string "TextGrid"
6. CREATOR exists and is PID service version (optional?)
100. exists

### Output

PID=21.11113/00-1734-0000-0002-2BA2-E,DATA=http://textgridrep.org/textgrid:khm9.0,FILESIZE=-1,PUBDATE=2011-12-30,AUTHORS=TextGrid,CREATOR=1734,CODE=1000111,INT=71 {1=2, 7=5233, 71=24287}

### Categories

“1“ means data is TextGrid conform; “0“ means, data is NOT TextGrid conform (see requirements above)

    DATA | METADATA | FILESIZE | CHECKSUM | PUBDATE | AUTHORS | CREATOR | HS_ADMIN
       1          0          0          0         1         1         1          1       = 143

Existing codes (so far...):
 
CODE=00000011 | INT=3
CODE=00001111 | INT=15
CODE=10001111 | INT=143


### Different PID metadata records (so far...)

https://hdl.handle.net/21.11113/0000-000B-CAC4-4?noredirect (dhrep)
    100	HS_ADMIN	2018-01-25 11:11:14Z 	handle=21.11113/USER02; index=1; [create hdl,delete hdl,read val,modify val,del val,add val,modify admin,del admin,add admin]

http://hdl.handle.net/11858/00-1734-0000-0006-4497-A?noredirect (tgrep)
    100	HS_ADMIN	2017-09-20 10:20:49Z 	handle=21.11113/USER01; index=1; [create hdl,delete hdl,read val,modify val,del val,add val,modify admin,del admin,add admin]
http://hdl.handle.net/11378/0000-0006-7326-E?noredirect (tgrep)
    100	HS_ADMIN	2017-08-01 21:18:53Z 	handle=0.NA/21.11113; index=200; [create hdl,delete hdl,read val,modify val,del val,add val,modify admin,del admin,add admin]
